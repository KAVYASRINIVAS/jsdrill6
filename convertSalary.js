function convertSalaryToInteger(data){
    let result=data.map((input) => {
        let salary=input.salary;
        input.salary=Number(salary.replaceAll('$',''));
        return input;
    });
    return result;
}

module.exports = {convertSalaryToInteger};
