function findCountrySalarySum(data){
    let salary=data.reduce((result,input) => {
        let salary=input.salary;
        let place=input.location;
        if(!result[place]){
            result[place]=salary;
        }else{
            result[place] +=salary;
        }
        return result;
    },{});
    return salary;
}

module.exports = {findCountrySalarySum};


