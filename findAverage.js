function averageOfSalaries(data) {      
    let sal = data.reduce((result, input) => {
        let salary = input.salary;
        let place = input.location;
        if (!result[place]) {
            result[place] = {
                'Salary': 0,
                'count': 0
            }
            result[place]['Salary'] = salary;
            result[place]['count'] = 1
        } else {
            result[place]['Salary'] += salary;
            result[place]['count'] += 1;

        }
        return result;
    }, {});

    let findAvg = Object.entries(sal).reduce((result, input) => {
        let country = input[0]
        let sum = input[1].Salary;
        let count = input[1].count;
        let average = Number(sum / count);
        result[country] = Number(average.toFixed(2));
        return result;
    }, {})
    return findAvg;
}

module.exports = {averageOfSalaries};

