const inputData=require('./input.json');

const web=require('./webDevelopers.js');
const convertSalary=require('./convertSalary.js');
const formatSalary=require('./correctedSalary.js');
const sum=require('./sumOfSalaries.js');
const countrySum=require('./findSumSalary.js');
const salaryAverage=require('./findAverage.js');


const t1=web.findWebDevelopers(inputData);
console.log(t1);

const t2=convertSalary.convertSalaryToInteger(inputData);
console.log(t2);

const t3=formatSalary.factorSalary(inputData);
console.log(t3);

const t4=sum.sumOfSalaries(inputData);
console.log(t4);

const t5=sum.sumOfCorrectedSalaries(inputData);
console.log(t5);

const t6=countrySum.findCountrySalarySum(inputData);
console.log(t6);

const t7=salaryAverage.averageOfSalaries(inputData);
console.log(t7);