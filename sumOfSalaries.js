function sumOfSalaries(data){
    let sum=data.reduce((addSalary,input) => addSalary+input.salary ,0);
    sum=Number(sum).toFixed(2);
    return sum;
}

function sumOfCorrectedSalaries(data){
    let result=data.reduce((total,input) => {
        total += input.corrected_salary;
        return total;
    },0);
    let sum=Number(result).toFixed(2);
    return sum;
}

module.exports ={sumOfSalaries,sumOfCorrectedSalaries};
