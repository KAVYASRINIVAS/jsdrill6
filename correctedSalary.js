function factorSalary(data){
    let salary=data.map((input) => {
            input['corrected_salary']=Number(input.salary)*10000;
            return input;
        });
    return salary
}

module.exports = {factorSalary};